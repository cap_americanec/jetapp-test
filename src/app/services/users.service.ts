import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { Observable, of } from 'rxjs';
import { HttpParams, HttpClient, HttpHeaders } from '@angular/common/http';
import {expand, map, reduce} from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})

export class UsersService {

  private _url: string = 'https://gne5ydctv0.execute-api.us-east-1.amazonaws.com/prod/';
 

  constructor(private _httpClient: HttpClient) {}

  getUsers(): Observable<User>
  {

     return this._httpClient.get<User>(this._url);
  }
}
