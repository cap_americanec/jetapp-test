import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { User } from 'src/app/models/user';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { UserModalComponent } from '../user-modal/user-modal.component';
import { interval, Subject, zip } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-custom-table',
  templateUrl: './custom-table.component.html',
  styleUrls: ['./custom-table.component.scss'],
})
export class CustomTableComponent implements OnInit {
  public dataSourse: User[] = [];
  public displayedColumns: string[] = ['name', 'lastName', 'age'];
  public numberOfRequests: number = 3;
  public isLoading: boolean = true;

  private users: User[] = [];

  constructor(
    private _usersService: UsersService,
    private _matDialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.initUsers();
  }

  public openModal(user: User): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.id = 'user-modal';
    dialogConfig.height = '300px';
    dialogConfig.width = '500px';
    dialogConfig.data = user;

    const modalDialog = this._matDialog.open(UserModalComponent, dialogConfig);
  }

  public initUsers(sliderValue?: number): void {
    this.users = [];
    this.dataSourse = [];
    this.isLoading = true;
    if(sliderValue) {
      this.numberOfRequests = sliderValue;
    }
    
    const usersObs = new Subject<number>();
    const getUsers = (i) => usersObs.next(i);
    const intervalObs = interval(350);
    zip(usersObs, intervalObs, (userId, i) => userId)
      .pipe(mergeMap(() => this._usersService.getUsers()))
      .subscribe((user) => {
        this.users.push(user);
        if (this.users.length == this.numberOfRequests) {
          this.dataSourse = this.users;
          this.isLoading = false;
        }
      });
    for (let i = 0; i < this.numberOfRequests; i++) {
      getUsers(i);
    }
  }
}
