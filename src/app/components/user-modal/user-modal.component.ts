import { Component, OnInit, Input, Inject } from '@angular/core';
import { User } from 'src/app/models/user';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.scss']
})
export class UserModalComponent implements OnInit {

  public form: FormGroup = new FormGroup({
    image_url: new FormControl(''),
    name: new FormControl(''),
    last_name: new FormControl(''),
    age: new FormControl('')
  })
  constructor(@Inject(MAT_DIALOG_DATA) public data: User, private _formBuilder: FormBuilder) { }

  ngOnInit(): void {
    if(this.data) {
      this.form = this._formBuilder.group({
        image_url: [{value: this.data.image_url, disabled: true}, Validators.required],
        name: [{value: this.data.name, disabled: true}, Validators.required],
        last_name: [{value: this.data.last_name, disabled: true}, Validators.required],
        age: [{value: this.data.age, disabled: true}, Validators.required],
      })
    }
  }

}
