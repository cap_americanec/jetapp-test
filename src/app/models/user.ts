export interface User {
    name: string;
    last_name: string;
    age: number;
    image_url: string;
}
